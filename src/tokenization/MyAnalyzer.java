package tokenization;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.ar.ArabicAnalyzer;
import org.apache.lucene.analysis.core.WhitespaceTokenizer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.util.Version;

public class MyAnalyzer extends Analyzer {
  
  private Version matchVersion;
  
  public MyAnalyzer(Version matchVersion) {
    this.matchVersion = matchVersion;
  }

  @Override
  protected TokenStreamComponents createComponents(String fieldName) {
    return new TokenStreamComponents(new WhitespaceTokenizer());
  }
  
  public static String directory = "/Users/asanchez75/Documents/workspace/Tokenization/input/";
  
  public static void main(String[] args) throws IOException {
    // text to tokenize
    //final String text = "我是中国人";
    //final String text = "وللرجل";
    //final String text = "On 2 December 1918, Pennsylvania steamed to anchor off Tompkinsville, New York.";
    
    ArrayList<String> files = listOfFiles();

    for (String filename : files) {
      
      String text = FileUtils.readFileToString(new File(directory + filename), "UTF-8");
      
      Version matchVersion = Version.LUCENE_6_2_1; // Substitute desired Lucene version for XY
     
      // CHANGE HERE!!!!!
      
     //MyRussianAnalyzer analyzer = new MyRussianAnalyzer(); // russian
     MyCJKAnalyzer analyzer = new MyCJKAnalyzer(); // chinese
      //MyArabicAnalyzer analyzer = new MyArabicAnalyzer(); // arabic      
      //MyEnglishAnalyzer analyzer = new MyEnglishAnalyzer(); // english
      
      TokenStream stream = analyzer.tokenStream("field", new StringReader(text));
      
      // get the CharTermAttribute from the TokenStream
      CharTermAttribute termAtt = stream.addAttribute(CharTermAttribute.class);

      try {
        stream.reset();
      
        // print all tokens until stream is exhausted
        while (stream.incrementToken()) {
          //System.out.println(termAtt.toString());
          FileUtils.writeStringToFile(new File(directory +  "/output/" + filename), termAtt.toString() + "\n", true);
        }
      
        stream.end();
        
      } finally {
        stream.close();
      }
      
    }
    
  }
 
  /**
   * Get list of filenames from folder containing scrapped text
   * @return
   */
  public static ArrayList<String> listOfFiles() {
    ArrayList<String> list = new ArrayList<String>();
    File folder = new File(directory);
    File[] listOfFiles = folder.listFiles();
 
        for (int i = 0; i < listOfFiles.length; i++) {
          if (listOfFiles[i].isFile()) {
            list.add(listOfFiles[i].getName());
          }
        }
    return list;
  }
}